﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using DotNetCoders.Manager.Manager;
using DotNetCoders.Model.Model;

namespace DotNetCoders.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerManager customerManager = new CustomerManager();
            char ch = 'Y';
            do
            {
                Console.WriteLine("Please enter which operation do you want to execute\n\tPress \n 1 for Add Customer\n 2 For Delete\n 3 For Update \n 4 For Show All\n 5 For SearchBy Id");
                int choice = Convert.ToInt32(Console.ReadLine());


                switch (choice)
                {

                    case 1:
                        var aCustomer = ACustomer();
                        if (customerManager.Add(aCustomer))
                        {
                            Console.WriteLine("Customer Saved");
                        }
                        else
                        {
                            Console.WriteLine("Customer not saved");
                        }
                        break;
                    case 2:
                        Console.WriteLine("Enter Customer Id to delete");
                        int id = Convert.ToInt32(Console.ReadLine());
                        if (customerManager.Delete(id))
                        {
                            Console.WriteLine("Customer Deleted");
                        }
                        else
                        {
                            Console.WriteLine("Customer not Deleted");
                        }
                        break;
                    case 3:
                        aCustomer = ACustomer();
                        if (customerManager.Update(aCustomer))
                        {
                            Console.WriteLine("Customer Updated");
                        }
                        else
                        {
                            Console.WriteLine("Customer not Updated");
                        }
                        break;
                    case 4:
                        List<Customer> customers = customerManager.GetAll();
                        string message = $"Name\t\tCode\t\tEmail\t\t\tContact\t\t\tAddress\t\tLoyalty Point\n";
                        if (customers.Count > 0)
                        {
                            foreach (Customer data in customers)
                            {
                                message += $"\n{data.Name}\t\t{data.Code}\t\t{data.Email}\t\t{data.Contact}\t\t{data.Address}\t\t{data.LoyaltyPoint}";
                            }
                            Console.WriteLine(message);
                        }
                        else
                        {
                            Console.WriteLine("Customer not found");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter Customer Id to search");
                        id = Convert.ToInt32(Console.ReadLine());
                        aCustomer = customerManager.GetBy(id);
                        if (aCustomer != null)
                        {
                            Console.WriteLine($"Name\t\tCode\t\tEmail\t\t\tContact\t\t\tAddress\t\tLoyalty Point");
                            Console.WriteLine($"\n{aCustomer.Name}\t\t{aCustomer.Code}\t\t{aCustomer.Email}\t\t{aCustomer.Contact}\t\t{aCustomer.Address}\t\t{aCustomer.LoyaltyPoint}");
                        }
                        else
                        {
                            Console.WriteLine("Customer not found");
                        }
                        break;
                    default:

                        break;
                }
                Console.WriteLine("Do you want to again those operation? Press Y for Yes N for No");
                ch = Convert.ToChar(Console.ReadLine());
            } while (ch == 'Y'|| ch == 'y');
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static Customer ACustomer()
        {
            Customer aCustomer = new Customer();
            cName:
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            if (!String.IsNullOrEmpty(name))
                aCustomer.Name = name;
            else
            {
                goto cName;
            }

            cCode:
            Console.WriteLine("Code: ");
            string code = Console.ReadLine();
            if (!String.IsNullOrEmpty(code))
                aCustomer.Code = code;
            else
            {
                goto cCode;
            }

            cEmail:
            Console.WriteLine("Email: ");
            string email = Console.ReadLine();
            if (!String.IsNullOrEmpty(email))
                aCustomer.Email = email;
            else
            {
                goto cEmail;
            }

            cAddress:
            Console.WriteLine("Address: ");
            string address = Console.ReadLine();
            if (!String.IsNullOrEmpty(address))
                aCustomer.Address = address;
            else
            {
                goto cAddress;
            }

            cContact:
            Console.WriteLine("Contact: ");
            string contact = Console.ReadLine();
            if (!String.IsNullOrEmpty(contact))
                aCustomer.Contact = contact;
            else
            {
                goto cContact;
            }

            cPoint:
            Console.WriteLine("Loyalty Point: ");
            string point = Console.ReadLine();
            if (!String.IsNullOrEmpty(point))
                aCustomer.LoyaltyPoint = Convert.ToInt32(point);
            else
            {
                goto cPoint;
            }

            return aCustomer;
        }
    }
}
