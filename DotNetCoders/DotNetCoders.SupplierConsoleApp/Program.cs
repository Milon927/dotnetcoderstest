﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetCoders.Model.Model;
using DotNetCoders.Manager.Manager;



namespace DotNetCoders.SupplierConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SupplierManager _supplierManager = new SupplierManager();

            //Add

            //Supplier supplier = new Supplier()
            //{
            //    Code="0001", Name="Moon", Address="Mohammadpur", Email="moon@gmail.com", Contact="01674657657", ContactPerson="Fariha"
            //};

            //if (_supplierManager.Add(supplier))
            //{
            //    Console.WriteLine("Saved!");
            //}
            //else
            //{
            //    Console.WriteLine("Not Saved!");
            //}

            //Console.ReadKey();

            
            char ch = 'Y';
            do
            {
                Console.WriteLine("Please enter which operation do you want to execute\n\tPress \n 1 for Add Supplier\n 2 For Delete\n 3 For Update \n 4 For Show All\n 5 For Search By Id");
                int choice = Convert.ToInt32(Console.ReadLine());


                switch (choice)
                {

                    case 1:
                        var aSupplier = ASupplier();
                        if (_supplierManager.Add(aSupplier))
                        {
                            Console.WriteLine("Saved!");
                        }
                        else
                        {
                            Console.WriteLine("Not saved!");
                        }
                        break;

                    case 2:
                        Console.WriteLine("Enter Supplier Id to delete");
                        int id = Convert.ToInt32(Console.ReadLine());
                        if (_supplierManager.Delete(id))
                        {
                            Console.WriteLine("Deleted!");
                        }
                        else
                        {
                            Console.WriteLine("Not deleted!");
                        }
                        break;

                    case 3:
                        aSupplier = ASupplier();
                       

                        if (_supplierManager.Update(aSupplier))
                        {
                            Console.WriteLine("Updated");
                        }
                        else
                        {
                            Console.WriteLine("Not Updated");
                        }
                        break;

                    case 4:
                        List<Supplier> suppliers = _supplierManager.GetAll();
                        string message = $"Name\t\tCode\t\tEmail\t\t\tContact\t\t\tAddress\t\tContact Person\n";
                        if (suppliers.Count > 0)
                        {
                            foreach (Supplier data in suppliers)
                            {
                                message += $"\n{data.Name}\t\t{data.Code}\t\t{data.Email}\t\t{data.Contact}\t\t{data.Address}\t\t{data.ContactPerson}";
                            }
                            Console.WriteLine(message);
                        }
                        else
                        {
                            Console.WriteLine("Supplier not found");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter Supplier Id to search");
                        id = Convert.ToInt32(Console.ReadLine());
                        aSupplier = _supplierManager.GetBy(id);
                        if (aSupplier != null)
                        {
                            Console.WriteLine($"Name\t\tCode\t\tEmail\t\t\tAddress\t\t\tContact\t\tContact Person");
                            Console.WriteLine($"\n{aSupplier.Name}\t\t{aSupplier.Code}\t\t{aSupplier.Email}\t\t{aSupplier.Address}\t\t{aSupplier.Contact}\t\t{aSupplier.ContactPerson}");
                        }
                        else
                        {
                            Console.WriteLine("Supplier not found");
                        }
                        break;
                    default:

                        break;
                }

                Console.WriteLine("Do you want to do these operations again? Press Y for Yes, N for No");

                ch = Convert.ToChar(Console.ReadLine());
            } while (ch == 'Y' || ch == 'y');
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();





        }


        private static Supplier ASupplier()
        {
            Supplier supplier = new Supplier();
        cName:
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            if (!String.IsNullOrEmpty(name))
                supplier.Name = name;
            else
            {
                goto cName;
            }

        cCode:
            Console.WriteLine("Code: ");
            string code = Console.ReadLine();
            if (!String.IsNullOrEmpty(code))
                supplier.Code = code;
            else
            {
                goto cCode;
            }

        cEmail:
            Console.WriteLine("Email: ");
            string email = Console.ReadLine();
            if (!String.IsNullOrEmpty(email))
                supplier.Email = email;
            else
            {
                goto cEmail;
            }

        cAddress:
            Console.WriteLine("Address: ");
            string address = Console.ReadLine();
            if (!String.IsNullOrEmpty(address))
                supplier.Address = address;
            else
            {
                goto cAddress;
            }

        cContact:
            Console.WriteLine("Contact: ");
            string contact = Console.ReadLine();
            if (!String.IsNullOrEmpty(contact))
                supplier.Contact = contact;
            else
            {
                goto cContact;
            }

        cContactPerson:
            Console.WriteLine("Contact Person: ");
            string contactPerson = Console.ReadLine();
            if (!String.IsNullOrEmpty(contactPerson))
                supplier.ContactPerson = contactPerson ;
            else
            {
                goto cContactPerson;
            }

            return supplier;
        }


    }
}
